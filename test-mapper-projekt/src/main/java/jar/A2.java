package jar;

public class A2 {
	private int a2;
	private String  exname;
	
	public int getA2() {
		return a2;
	}
	public void setA2(int a2) {
		this.a2 = a2;
	}
	public String getExname() {
		return exname;
	}
	public void setExname(String exname) {
		this.exname = exname;
	}
	@Override
	public String toString() {
		return "A2 [a2=" + a2 + ", exname=" + exname + "]";
	}
	
	

}
