package jar;

import fr.xebia.extras.selma.Field;
import fr.xebia.extras.selma.InheritMaps;
import fr.xebia.extras.selma.Mapper;
import fr.xebia.extras.selma.Maps;

@Mapper(
		withCustomFields = {
		        @Field({"A1.a1id", "A2.a2"}),
		        @Field({"A1.name", "A2.exname"})
	    })
public interface A1A1Mapper {
	
    // This will build a fresh new OrderDto
	A1 asA1(A2 in);

     // This will update the given order
    A2 asA2(A1 in);

}
