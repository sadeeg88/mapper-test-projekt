package jar;

import fr.xebia.extras.selma.Selma;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	A1A1Mapper mymapper = Selma.builder(A1A1Mapper.class).build();
    	A1 a1 = new A1();
    	a1.setA1id(12);
    	a1.setName("Sexy");
    	
    	A2 a12 = mymapper.asA2(a1);
    	
    	System.out.println(a1.toString());
    	System.out.println(a12.toString());
    	
    	
    	
    }
}
