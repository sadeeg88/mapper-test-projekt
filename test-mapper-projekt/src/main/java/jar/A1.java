package jar;

public class A1 {
	private int a1id;
	private String name;
	
	public int getA1id() {
		return a1id;
	}
	public void setA1id(int a1id) {
		this.a1id = a1id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "A1 [a1id=" + a1id + ", name=" + name + "]";
	}

	
	

}
