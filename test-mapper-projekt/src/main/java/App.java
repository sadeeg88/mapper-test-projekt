

import fr.xebia.extras.selma.Selma;
import jar.A1;
import jar.A1A1Mapper;
import jar.A2;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	A1A1Mapper mymapper = Selma.builder(A1A1Mapper.class).build();
    	A1 a1 = new A1();
    	a1.setA1id(12);
    	a1.setName("Sexy");
    	
    	A2 a12 = mymapper.asA2(a1);
    	
    	System.out.println(a1.toString());
    	System.out.println(a12.toString());
    	
    	
    	A2 a2 = new A2();
    	a2.setA2(44);
    	a2.setExname("Men");
    	A1 a21 = mymapper.asA1(a2);
    	
    	System.out.println(a2.toString());
    	System.out.println(a21.toString());
    	
    	
    	
    	
    }
}
